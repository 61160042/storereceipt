/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Customer;
import model.Product;
import model.Receipt;
import model.User;
import model.ReceiptDetail;

/**
 *
 * @author cherr
 */
public class ReceiptDao implements DaoInterface<Receipt> {

    @Override
    public int add(Receipt object) {
        Connection conn = null;
        String dbPath = "./db/store.db";
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        //insert
        try {
            String sql = "INSERT INTO receipt (customer_id,user_id,total) VALUES (?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getSeller().getId());
            stmt.setInt(2, object.getCustomer().getId());
            stmt.setDouble(3, object.getTotal());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
                object.setId(id);
            }
            for(ReceiptDetail r : object.getReceiptDetail()){
                String sqlDetail = "INSERT INTO receipt_datail (receipt_id,product_id,price,amount) VALUES (?,?,?,?);";
                PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
                stmtDetail.setInt(1, r.getReceipt().getId());
                stmtDetail.setInt(2, r.getProduct().getId());
                stmtDetail.setDouble(3, r.getPrice());
                stmtDetail.setInt(4, r.getAmount());
                int rowDetai = stmtDetail.executeUpdate();
                ResultSet resultDetai = stmt.getGeneratedKeys();
                if (resultDetai.next()) {
                    id = resultDetai.getInt(1);
                    r.setId(id);
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error : to create receipt");
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Receipt> getAll() {
        Connection conn = null;
        String dbPath = "./db/store.db";
        Database db = Database.getInstance();
        conn = db.getConnection();
        ArrayList<Receipt> list = new ArrayList<>();
        //select
        try {
            String sql = "SELECT r.id as id,\n"
                    + "       created,\n"
                    + "       customer_id,\n"
                    + "       c.name as customer_name,\n"
                    + "       c.tel as customer_tel,\n"
                    + "       user_id,\n"
                    + "       u.name as user_name,\n"
                    + "       u.tel as user_tel,\n"
                    + "       total\n"
                    + " FROM receipt r, customer c ,  user u\n"
                    + " WHERE r.customer_id = c.id AND r.user_id = u.id";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                System.out.println(result.getString("created"));
                int id = result.getInt("id");
                java.util.Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("created"));
                int customerId = result.getInt("customer_id");
                String customerName = result.getString("customer_name");
                String customerTel = result.getString("customer_tel");
                int userId = result.getInt("user_id");
                String userName = result.getString("user_name");
                String userTel = result.getString("user_tel");
                double total = result.getDouble("total");
                Receipt receipt = new Receipt(id, created, new User(userId, userName, userTel),
                        new Customer(customerId, customerName, customerTel));
                list.add(receipt);
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all receipt!!!" + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error : Date parsing all receipt!!!" + ex.getMessage());
        }

        db.close();
        return list;
    }

    @Override
    public Receipt get(int id) {
        Connection conn = null;
        String dbPath = "./db/store.db";
        Database db = Database.getInstance();
        conn = db.getConnection();
        ArrayList<Receipt> list = new ArrayList<>();
        //select
        try {
            String sql = "SELECT r.id as id,\n"
                    + "       created,\n"
                    + "       customer_id,\n"
                    + "       c.name as customer_name,\n"
                    + "       c.tel as customer_tel,\n"
                    + "       user_id,\n"
                    + "       u.name as user_name,\n"
                    + "       u.tel as user_tel,\n"
                    + "       total\n"
                    + " FROM receipt r, customer c ,  user u\n"
                    + " WHERE r.id = ?  AND r.customer_id = c.id AND r.user_id = u.id";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                int rid = result.getInt("id");
                java.util.Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("created"));
                int customerId = result.getInt("customer_id");
                String customerName = result.getString("customer_name");
                String customerTel = result.getString("customer_tel");
                int userId = result.getInt("user_id");
                String userName = result.getString("user_name");
                String userTel = result.getString("user_tel");
                double total = result.getDouble("total");
                Receipt receipt = new Receipt(id, created, new User(userId, userName, userTel),
                        new Customer(customerId, customerName, customerTel));
                getReceiptDetail(conn, id, receipt);
                return receipt;
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select receipt id " + id);
        } catch (ParseException ex) {
            System.out.println("Error : Date parsing all receipt!!!" + ex.getMessage());
        }
        return null;
    }

    private void getReceiptDetail(Connection conn, int id, Receipt receipt) throws SQLException {
        String sqlDetail = "SELECT rd.id as id,\n" +
                "       receipt_id,\n" +
                "       product_id,\n" +
                "       p.name as product_name,\n"+
                "       p.price as product_price,\n"+
                "       rd.price as price,\n" +
                "       amount\n" +
                "  FROM receipt_datail rd,product p\n"+
                "  WHERE receipt_id = ? AND rd.product_id = p.id";
        PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
        stmtDetail.setInt(1, id);
        ResultSet resultDetail = stmtDetail.executeQuery();
        while(resultDetail.next()){
            int receiptId = resultDetail.getInt("id");
            int productId = resultDetail.getInt("product_id");
            String productName = resultDetail.getString("product_name");
            double productPrice = resultDetail.getDouble("product_price");
            double price = resultDetail.getDouble("price");
            int amount = resultDetail.getInt("amount");
            Product product = new Product(productId, productName, productPrice);
            receipt.addReceiptDetail(receiptId,product, amount,price);
        }
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        String dbPath = "./db/store.db";
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        //insert
        try {
            String sql = "DELETE FROM receipt WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error : Unable to Ddelete id " + id);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Receipt object) {
//       Connection conn = null;
//        String dbPath = "./db/store.db";
//        Database db = Database.getInstance();
//        conn = db.getConnection();
//        int row=0;
//        try {
//            String sql = "UPDATE product SET name = ?,price = ? WHERE id = ?";
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, object.getName());
//            stmt.setDouble(2, object.getPrice());
//            stmt.setInt(3, object.getId());
//            row = stmt.executeUpdate();
//        } catch (SQLException ex) { 
//            Logger.getLogger(ReceiptDao.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        
//        db.close();
        return 0;
    }

    public static void main(String[] args) {
        Product p1 = new Product(1, "Americano1", 40);
        Product p2 = new Product(2, "Green Tea", 30);
        User seller = new User(1, "Rujirada Norasarn", "0818888612");
        Customer customer = new Customer(1, "Yaya", "066613265");
        Receipt receipt = new Receipt(seller, customer);
        receipt.addReceiptDetail(p1, 1);
        receipt.addReceiptDetail(p2, 1);
        ReceiptDao dao = new ReceiptDao();
//        System.out.println("id = " + dao.add(receipt));
//        System.out.println("Receipt after add : "+receipt);
//        System.out.println("Get All "+dao.getAll());
        
//        Receipt newReceipt = dao.get(receipt.getId());
//        System.out.println("New Receipt "+newReceipt);
        
        dao.delete(30);
    }
}
