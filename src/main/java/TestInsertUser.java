
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;
import model.User;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author cherr
 */
public class TestInsertUser {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Connection conn = null;
        String dbPath = "./db/store.db";
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        //insert
        try {
            String sql = "INSERT INTO user (password,tel,name)VALUES (?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            User user = new User(-1, "password", "0868896789", "cherry");
            stmt.setString(1, user.getPassword());
            stmt.setString(2, user.getTel());
            stmt.setString(3, user.getName());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            int id = -1;
            if(result.next()){
                id = result.getInt(1);
            }
            System.out.println("Affect row "+row+" id: "+id);
        } catch (SQLException ex) {
            Logger.getLogger(TestInsertProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        db.close();
    }
    
}
