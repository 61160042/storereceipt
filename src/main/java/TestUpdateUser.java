
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author cherr
 */
public class TestUpdateUser {
    public static void main(String[] args) {
        Connection conn = null;
        String dbPath = "./db/store.db";
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        //insert
        try {
            String sql = "UPDATE user SET name = ?, tel = ?, password = ? WHERE id =?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "c cherry");
            stmt.setString(2, "0899999999");
            stmt.setString(3, "1010100");
            stmt.setInt(4, 4);
            int row = stmt.executeUpdate();
            System.out.println("Affect row "+row);
        } catch (SQLException ex) {
            Logger.getLogger(TestInsertProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        db.close();
    }
}
