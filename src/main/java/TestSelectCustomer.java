
import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.Product;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author cherr
 */
public class TestSelectCustomer {
    public static void main(String[] args) {
        Connection conn = null;
        String dbPath = "./db/store.db";
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        //select
        try {
            String sql = "SELECT id, name, tel FROM customer";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                Customer customer = new Customer(id, name, tel);
                System.out.println(customer);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        db.close();
    }
}
